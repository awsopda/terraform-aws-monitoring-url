locals {
    checks_name = length(var.checks) == 0 ? "${var.project_name}-disabled" : lookup(var.checks[0], "name", "")
}

data "aws_iam_policy_document" "lambda" {

  statement {
    sid = "1"

    actions = [
      "cloudwatch:PutMetricData",
    ]

    resources = [
      "*"
    ]
  }
}

module "lambda" {
  source = "./lambda"

  function_name                  = "${local.checks_name}-http_checks"
  description                    = "Completes HTTP check for a given URL"
  handler                        = "lambda.lambda_handler"
  runtime                        = "python3.8"
  timeout                        = 300
  reserved_concurrent_executions = 1

  source_path = "${path.module}/http_check-lambda/"

  policy     = {
      json = data.aws_iam_policy_document.lambda.json
  }

  vpc_config = var.vpc_config
}

resource "aws_lambda_permission" "allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = module.lambda.function_arn
  principal     = "events.amazonaws.com"
}
