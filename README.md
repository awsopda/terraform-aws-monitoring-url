## Example Usage

```hcl
module "http_check" {
  source = "monitoring-module"
  checks = [
    {
      name = "Google"
      description = "Service Name: Service Name will be in the email\nEnvironment Type: Prod"
      url = "https://www.google.com"
      rate = "1 minute"
      threshold = "0.5"
      valid_status_codes = "200,301,301"
      response_string = "shetrades"
      alarm_actions = "arn:aws:sns:eu-west-1:1234567890:sns-monitoring"
      ok_actions = "arn:aws:sns:eu-west-1:1234567890:sns-monitoring"
    }
  ]
}
```

The map for each check can have the following parameters:
