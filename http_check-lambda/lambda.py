"""
Checks url and store response time and status code
"""

import boto3
import logging
import requests
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

client = boto3.client('cloudwatch')
logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(event, context):
    logger.info('Event:' + str(event))
    headers = {'User-Agent': 'ICC Cloud Web Hosting Monitor'}

    response = requests.get(event['url'], headers=headers, verify=False)

    logger.info("Putting HTTP Status")
    got_valid_status_code = str(response.status_code) in event['valid_status_codes'].split(",") and event['response_string'] in response.text

    client.put_metric_data(
      Namespace='HTTP Checks',
      MetricData=[
          {
              'MetricName': 'WebStatus',
              'Dimensions': [
                  {
                      'Name': 'URL',
                      'Value': event["url"],
                  }
              ],
              'Value': 0 if got_valid_status_code else 1,
          }
      ]
    )

    logger.info("Putting HTTP response time")
    client.put_metric_data(
      Namespace='HTTP Checks',
      MetricData=[
          {
              'MetricName': 'WebResponseTime',
              'Dimensions': [
                  {
                      'Name': 'URL',
                      'Value': event["url"],
                  }
              ],
              'Value': response.elapsed.total_seconds(),
          }
      ]
    )
